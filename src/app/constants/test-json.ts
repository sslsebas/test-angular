export class Constants {
  public static items =[
    {
        "_id": "5fc692b0a95e5f7b1bffb621",
        "index": 0,
        "isActive": true,
        "balance": "$1,715.00",
        "picture": "http://placehold.it/32x32",
        "age": 34,
        "eyeColor": "green",
        "name": "Pansy Sykes",
        "gender": "female",
        "company": "MONDICIL",
        "email": "pansysykes@mondicil.com",
        "phone": "+1 (854) 516-3538",
        "address": "710 Lawrence Avenue, Sedley, Wyoming, 2735",
        "about": "Officia ea labore ipsum dolor nisi. Est sit eiusmod enim sit dolore nulla incididunt duis exercitation aliquip sit laboris. Nostrud proident quis est eiusmod eu eiusmod est. Eiusmod elit ullamco fugiat cupidatat aliqua magna do sunt eu amet est anim. Deserunt sint deserunt minim amet in excepteur nisi nulla laborum cupidatat et consequat ullamco velit. Ad esse laboris ut et Lorem adipisicing elit elit Lorem excepteur do incididunt cillum.\r\n",
        "registered": "2015-11-03T02:08:37 +05:00",
        "latitude": 20.759734,
        "longitude": 23.888882,
        "tags": [
            "commodo",
            "amet",
            "ut",
            "consequat",
            "irure",
            "incididunt",
            "consectetur"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Kathleen Moss"
            },
            {
                "id": 1,
                "name": "Matthews Howe"
            },
            {
                "id": 2,
                "name": "Willa Frye"
            }
        ]
    },
    {
        "_id": "5fc692b0b87120a8bd794806",
        "index": 1,
        "isActive": false,
        "balance": "$1,790.36",
        "picture": "http://placehold.it/32x32",
        "age": 24,
        "eyeColor": "green",
        "name": "Waters Morales",
        "gender": "male",
        "company": "KATAKANA",
        "email": "watersmorales@katakana.com",
        "phone": "+1 (927) 479-2738",
        "address": "810 Osborn Street, Ernstville, Ohio, 6652",
        "about": "Eu adipisicing pariatur cupidatat adipisicing quis deserunt labore consectetur qui. Ullamco nostrud reprehenderit mollit minim velit. Esse mollit exercitation quis anim. Aliquip pariatur eiusmod esse id fugiat eiusmod labore velit eu sit. Ad aute amet adipisicing nulla fugiat enim excepteur commodo id nostrud cillum ad. Et cillum nostrud dolore do quis.\r\n",
        "registered": "2014-10-15T05:16:10 +05:00",
        "latitude": -82.148988,
        "longitude": 93.549735,
        "tags": [
            "deserunt",
            "sit",
            "duis",
            "voluptate",
            "deserunt",
            "sint",
            "non"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Vonda Guy"
            },
            {
                "id": 1,
                "name": "Erika Donovan"
            },
            {
                "id": 2,
                "name": "Holman Carney"
            }
        ]
    },
    {
        "_id": "5fc692b06bfb459e02d7fab4",
        "index": 2,
        "isActive": false,
        "balance": "$2,404.62",
        "picture": "http://placehold.it/32x32",
        "age": 26,
        "eyeColor": "green",
        "name": "Bianca Farley",
        "gender": "female",
        "company": "WEBIOTIC",
        "email": "biancafarley@webiotic.com",
        "phone": "+1 (830) 535-3909",
        "address": "850 Montieth Street, Avoca, Connecticut, 9072",
        "about": "Ad laboris anim sit in proident duis tempor laborum in laboris tempor non reprehenderit. Ut excepteur et ullamco ea ea sint nisi fugiat. Officia aliquip sint non magna fugiat nostrud pariatur commodo laborum deserunt ad. Lorem est ullamco in sunt fugiat incididunt pariatur est quis ad ut. Voluptate cupidatat aute velit consequat enim sint anim deserunt Lorem occaecat labore velit adipisicing ullamco. Aute incididunt aute commodo cillum.\r\n",
        "registered": "2019-10-22T10:58:25 +05:00",
        "latitude": 69.95062,
        "longitude": 90.159343,
        "tags": [
            "do",
            "aliquip",
            "qui",
            "consequat",
            "veniam",
            "veniam",
            "qui"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Lauren Gamble"
            },
            {
                "id": 1,
                "name": "Esther Ortega"
            },
            {
                "id": 2,
                "name": "Phyllis Campos"
            }
        ]
    },
    {
        "_id": "5fc692b0570e63ba5cc53d6a",
        "index": 3,
        "isActive": false,
        "balance": "$3,635.71",
        "picture": "http://placehold.it/32x32",
        "age": 33,
        "eyeColor": "green",
        "name": "Stella Avila",
        "gender": "female",
        "company": "GEOLOGIX",
        "email": "stellaavila@geologix.com",
        "phone": "+1 (809) 517-2881",
        "address": "240 Oxford Walk, Vandiver, Louisiana, 2099",
        "about": "Excepteur minim laborum adipisicing ipsum qui amet eu do ut eiusmod proident excepteur reprehenderit. Ullamco sunt anim quis do enim id. Labore proident anim pariatur sint incididunt minim tempor quis velit duis fugiat labore. Consectetur minim elit quis reprehenderit irure commodo.\r\n",
        "registered": "2019-11-24T09:32:04 +05:00",
        "latitude": 5.082023,
        "longitude": -176.767301,
        "tags": [
            "aute",
            "cupidatat",
            "cillum",
            "quis",
            "deserunt",
            "fugiat",
            "non"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Hattie Shepard"
            },
            {
                "id": 1,
                "name": "Doreen French"
            },
            {
                "id": 2,
                "name": "Cecilia Santos"
            }
        ]
    },
    {
        "_id": "5fc692b0091c0beeed6448c6",
        "index": 4,
        "isActive": false,
        "balance": "$2,539.46",
        "picture": "http://placehold.it/32x32",
        "age": 28,
        "eyeColor": "brown",
        "name": "Navarro Cantrell",
        "gender": "male",
        "company": "BITREX",
        "email": "navarrocantrell@bitrex.com",
        "phone": "+1 (895) 538-3366",
        "address": "298 Sunnyside Avenue, Hailesboro, New Mexico, 8068",
        "about": "Fugiat ad cillum anim amet elit adipisicing ad officia deserunt dolore ex dolor occaecat ipsum. Nulla pariatur sit ut irure in officia voluptate sint ad voluptate. Tempor elit Lorem officia fugiat anim labore adipisicing. Amet consectetur voluptate excepteur aute fugiat nulla consectetur. Dolore minim commodo nostrud veniam tempor amet.\r\n",
        "registered": "2019-03-27T12:07:07 +05:00",
        "latitude": 86.632737,
        "longitude": 15.186139,
        "tags": [
            "et",
            "exercitation",
            "magna",
            "aute",
            "non",
            "minim",
            "et"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Greene Dodson"
            },
            {
                "id": 1,
                "name": "Susie Howell"
            },
            {
                "id": 2,
                "name": "Marsha Case"
            }
        ]
    },
    {
        "_id": "5fc692b0c1775cd546d5112b",
        "index": 5,
        "isActive": false,
        "balance": "$1,751.84",
        "picture": "http://placehold.it/32x32",
        "age": 38,
        "eyeColor": "green",
        "name": "Ayala Collins",
        "gender": "male",
        "company": "OMNIGOG",
        "email": "ayalacollins@omnigog.com",
        "phone": "+1 (985) 600-2172",
        "address": "535 Just Court, Wright, Palau, 3400",
        "about": "Ut ullamco commodo est enim reprehenderit occaecat nisi est duis est veniam consequat. Commodo nisi magna commodo ullamco ad ex minim ex consequat in irure qui duis fugiat. Proident laboris esse ea anim magna laborum ut veniam dolore reprehenderit dolor dolore adipisicing cillum. Eu aliquip sunt veniam excepteur. Sunt pariatur occaecat sit labore. Esse sint cillum id irure eu.\r\n",
        "registered": "2017-01-01T05:34:27 +05:00",
        "latitude": 66.177368,
        "longitude": 156.609437,
        "tags": [
            "culpa",
            "labore",
            "est",
            "tempor",
            "do",
            "aute",
            "velit"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Rose Velasquez"
            },
            {
                "id": 1,
                "name": "Grace Vasquez"
            },
            {
                "id": 2,
                "name": "Mcfarland Gilliam"
            }
        ]
    },
    {
        "_id": "5fc692b085368b67f2b55982",
        "index": 6,
        "isActive": true,
        "balance": "$1,484.50",
        "picture": "http://placehold.it/32x32",
        "age": 35,
        "eyeColor": "blue",
        "name": "Puckett Figueroa",
        "gender": "male",
        "company": "GINK",
        "email": "puckettfigueroa@gink.com",
        "phone": "+1 (951) 549-3996",
        "address": "915 Creamer Street, National, Arkansas, 5904",
        "about": "Reprehenderit tempor ea velit commodo et. Sint ipsum mollit anim Lorem cupidatat est amet dolor in pariatur elit elit. Sunt et consequat eiusmod eiusmod laboris reprehenderit pariatur veniam magna minim non ut.\r\n",
        "registered": "2018-11-08T04:52:05 +05:00",
        "latitude": -70.674699,
        "longitude": -120.549997,
        "tags": [
            "reprehenderit",
            "sit",
            "amet",
            "reprehenderit",
            "ipsum",
            "qui",
            "nulla"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Arnold Odonnell"
            },
            {
                "id": 1,
                "name": "Mona Suarez"
            },
            {
                "id": 2,
                "name": "Marina Moon"
            }
        ]
    }
]

 }
