import { Component, OnInit } from '@angular/core';
// import { constants } from 'buffers';
import { Constants } from 'src/app/constants/test-json';


@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  constructor() { }
  staticItemsList = Constants.items
  filteredList = this.staticItemsList
  showActives: boolean = true
  tags = ["commodo", "amet", "ut", "consequat", "irure", "incididunt", "consectetur", "deserunt", "sit", "duis", "voluptate", "sint", "non", "do", "aliquip", "qui", "veniam", "aute", "cupidatat", "cillum", "quis", "fugiat", "et", "exercitation", "magna", "minim", "culpa", "labore", "est", "tempor", "velit", "reprehenderit", "ipsum", "nulla"]
  ngOnInit(): void {
  }

  useFilter(input: any) {
    console.log(input)
    this.filteredList = this.staticItemsList.filter(item => item.name.toLowerCase().includes(input.target.value.toLowerCase()))
  }

  useTagFilter(change: any) {
    console.log(change)
    if (change.target.value === "All") {
      this.filteredList = this.staticItemsList
    }
    else {
      this.filteredList = this.staticItemsList.filter(item => item.tags.some(tag => tag === change.target.value))
    }

  }


}
